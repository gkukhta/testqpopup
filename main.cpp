// standard C++ header:
#include <iostream>
#include <string>

// Qt header:
#include <QApplication>
#include <QLabel>
#include <QMainWindow>
#include <QTimer>

using namespace std;

int main(int argc, char **argv)
{
  cout << QT_VERSION_STR << endl;
  // main application
#undef qApp // undef macro qApp out of the way
  QApplication qApp(argc, argv);
  // setup GUI
  QMainWindow qWin;
  qWin.setFixedSize(640, 400);
  qWin.show();
  // setup popup
  QLabel qPopup(QString::fromLatin1("Some text"),
    &qWin,
    Qt::SplashScreen | Qt::WindowStaysOnTopHint);
  QPalette qPalette = qPopup.palette();
  qPalette.setBrush(QPalette::Background, QColor(0xff, 0xe0, 0xc0));
  qPopup.setPalette(qPalette);
  qPopup.setFrameStyle(QLabel::Raised | QLabel::Panel);
  qPopup.setAlignment(Qt::AlignCenter);
  qPopup.setFixedSize(320, 200);
  qPopup.show();
  // setup timer
  QTimer::singleShot(1000,
    [&qPopup]() {
    qPopup.setText(QString::fromLatin1("Closing in 3 s"));
  });
  QTimer::singleShot(2000,
    [&qPopup]() {
    qPopup.setText(QString::fromLatin1("Closing in 2 s"));
  });
  QTimer::singleShot(3000,
    [&qPopup]() {
    qPopup.setText(QString::fromLatin1("Closing in 1 s"));
  });
  QTimer::singleShot(4000, &qPopup, &QLabel::hide);
  // run application
  return qApp.exec();
}
